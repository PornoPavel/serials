<?php

namespace app\widgets\FilmItem;

use yii\bootstrap\Widget;

class FilmItem extends Widget
{
    public function run()
    {
        return $this->render('fileItem', $this->options);
    }
}