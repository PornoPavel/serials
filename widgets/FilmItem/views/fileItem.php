<div class="row" style="padding-top: 40px">
    <div class="left-part">
        <img class="logo" src="<?= $imgSrc ?>">
    </div>
    <div class="right-part">
        <a class="title" href="<?= $hrefSrc ?>"><?= $name ?></a>
        <p><?= $description ?></p>
        <p><?= $lastSeason ?>й сезон</p>
        <p><?= $lastSeries ?>я серия</p>
    </div>
</div>
