<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string name
 * @property string description
 * @property string image
 * @property integer year
 * @property string status_id
 * @property string studio_id
 */
class Serial extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%serials}}';
    }

    public function getStatus()
    {
        return $this->hasOne('app\models\Status', ['id' => 'status_id']);
    }

    public function getStudio()
    {
        return $this->hasOne('app\models\Studio', ['id' => 'studio_id']);
    }

    public function getLastSeries()
    {
        return $this->hasOne('app\models\Season', ['serial_id' => 'id'])->orderBy(['season_num' => SORT_DESC])->joinWith('lastSeries');
    }

    public function getGenres()
    {
        return $this->hasMany('app\models\SerialGenre', ['serial_id' => 'id'])->joinWith('genre');
    }

    public function getCountries()
    {
        return $this->hasMany('app\models\SerialCountry', ['serial_id' => 'id'])->joinWith('country');
    }

    public function getProducer()
    {
        return $this->hasOne('app\models\Season', ['serial_id' => 'id'])->with('lastSeries')->with('producer');
    }
}