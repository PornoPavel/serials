<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer serial_id
 */
class UserFavorite extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%users_favorites}}';
    }

    public function getUser()
    {
        return $this->hasOne('app\models\User', ['id' => 'user_id']);
    }

    public function getSeries()
    {
        return $this->hasOne('app\models\Serial', ['id' => 'serial_id']);
    }
}