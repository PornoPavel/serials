<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string name
 * @property string description
 * @property integer season_id
 * @property string release_date
 * @property integer series
 */
class Series extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%series}}';
    }

    public function getSeason()
    {
        return $this->hasOne('app\models\Season', ['id' => 'season_id']);
    }

    public function getSeasonSerial()
    {
        return $this->hasOne('app\models\Season', ['id' => 'season_id'])->with('serial');
    }

    public function getSeasonSerialLast()
    {
        return $this->hasOne('app\models\Season', ['id' => 'season_id'])->with('lastSeries');
    }

    public function getProducer()
    {
        return $this->hasOne('app\models\SeriesPeople', ['series_id' => 'id'])->where(['=', 'role_id', 1]);
    }
}