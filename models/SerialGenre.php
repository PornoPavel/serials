<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer genre_id
 * @property integer serial_id
 */
class SerialGenre extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%serials_genres}}';
    }

    public function getGenre()
    {
        return $this->hasOne('app\models\Genres', ['id' => 'genre_id']);
    }

    public function getSerial()
    {
        return $this->hasOne('app\models\Serial', ['serial_id' => 'id']);
    }
}