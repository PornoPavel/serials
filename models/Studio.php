<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string name
 */
class Studio extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%studios}}';
    }
}