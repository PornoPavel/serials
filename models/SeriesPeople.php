<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer people_id
 * @property integer series_id
 * @property integer role_id
 */
class SeriesPeople extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%series_peoples}}';
    }

    public function getPeople()
    {
        return $this->hasOne('app\models\People', ['id' => 'people_id']);
    }

    public function getSeries()
    {
        return $this->hasOne('app\models\Series', ['id' => 'series_id']);
    }

    public function getRole()
    {
        return $this->hasOne('app\models\Role', ['id' => 'role_id']);
    }
}