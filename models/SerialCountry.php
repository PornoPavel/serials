<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer country_id
 * @property integer serial_id
 */
class SerialCountry extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%serials_countries}}';
    }

    public function getCountry()
    {
        return $this->hasOne('app\models\Country', ['id' => 'country_id']);
    }

    public function getSerial()
    {
        return $this->hasOne('app\models\Serial', ['id' => 'serial_id']);
    }
}