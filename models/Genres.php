<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string name
 */
class Genres extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%genres}}';
    }
}