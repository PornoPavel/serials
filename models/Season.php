<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer serial_id
 * @property integer season_num
 */
class Season extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%seasons}}';
    }

    public function getSeries()
    {
        return $this->hasMany('app\models\Series', ['season_id' => 'id']);
    }

    public function getLastSeries()
    {
        return $this->hasOne('app\models\Series', ['season_id' => 'id'])->orderBy(['release_date' => SORT_DESC]);
    }

    public function getSerial()
    {
        return $this->hasOne('app\models\Serial', ['studio_id' => 'id']);
    }

    public function getProducer()
    {
        return $this->getLastSeries()->with('producer');
    }
}