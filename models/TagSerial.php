<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer serial_id
 * @property integer tag_id
 */
class TagSerial extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tag_serial}}';
    }

    public function serial()
    {
        return $this->hasOne('app\models\Serial', ['id' => 'serial_id']);
    }

    public function tag()
    {
        return $this->hasOne('app\models\Tag', ['id' => 'tag_id']);
    }
}