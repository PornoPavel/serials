<?php

namespace app\models\form;

use yii\base\Model;

class AddSeasonForm extends Model
{
    public $serial_id;
    public $season_num;

    public function rules()
    {
        return [
            [['serial_id', 'season_num',], 'required'],
            [['serial_id', 'season_num',], 'integer'],
        ];
    }
}