<?php

namespace app\models\form;

use app\models\User;
use Yii;
use yii\base\Model;

class RegisterFrom extends Model
{
    public $username;
    public $password;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email address has already been taken.'],
        ];
    }

    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->password = $this->password;
            $user->save();
            Yii::$app->user->login($user);
        }

        return false;
    }
}