<?php

namespace app\models\form;

use yii\base\Model;

class AddSeriesForm extends Model
{
    public $name;
    public $description;
    public $season_id;
    public $release_date;
    public $series;

    public function rules()
    {
        return [
            [['name', 'description', 'season_id', 'release_date', 'series',], 'required'],
            [['season_id', 'series',], 'integer'],
        ];
    }
}