<?php

namespace app\models\form;

use yii\base\Model;

class AddPeopleForm extends Model
{
    public $FIO;
    public $date_birth;
    public $biography;

    public function rules()
    {
        return [
            [['FIO', 'date_birth', 'biography'], 'required'],
        ];
    }
}