<?php

namespace app\models\form;

use yii\base\Model;

class AddSerialForm extends Model
{
    public $name;
    public $genres;
    public $tags;
    public $year;
    public $country_id;
    public $status_id;
    public $studio_id;
    public $description;
    public $image;

    public function rules()
    {
        return [
            [['name', 'genres', 'year', 'country_id', 'status_id', 'studio_id', 'description', 'tags'], 'required'],
            [['year', 'status_id', 'studio_id',], 'integer'],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

}