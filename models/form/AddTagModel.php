<?php

namespace app\models\form;

use yii\base\Model;

class AddTagModel extends Model
{
    public $name;

    public function rules()
    {
        return [
            [['name',], 'required'],
        ];
    }
}