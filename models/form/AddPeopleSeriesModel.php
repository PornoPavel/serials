<?php


namespace app\models\form;


use yii\base\Model;

class AddPeopleSeriesModel extends Model
{
    public $people_id;
    public $series_id;
    public $role_id;

    public function rules()
    {
        return [
            [['people_id', 'series_id', 'role_id'], 'required'],
        ];
    }
}