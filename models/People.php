<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string FIO
 * @property string date_birth
 * @property string biography
 */
class People extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%peoples}}';
    }
}