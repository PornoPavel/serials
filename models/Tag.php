<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property string name
 */
class Tag extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tags}}';
    }
}