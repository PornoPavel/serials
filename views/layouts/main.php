<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerCssFile('css/bootstrap/bootstrap-grid.css') ?>
    <?php $this->registerCssFile('css/Chart.css') ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => 'Каталог', 'url' => ['/site/index']],
    ];

    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => 'Войти', 'url' => ['/site/login']];
        $items[] = ['label' => 'Регистрация', 'url' => ['/site/register']];
    } else {
        $items[] = ['label' => 'Избранное', 'url' => ['/favorite/index']];
        $items[] = ['label' => 'Статистика', 'url' => ['/statistic/index']];

        if (Yii::$app->user->identity->is_admin == 1) {
            $items[] = ['label' => 'Добавить', 'url' => ['/site/adds']];
        }

        $userName = Yii::$app->user->identity->username;
        $form = Html::submitButton("Выйти ($userName)", ['class' => 'btn btn-link logout']);
        $form = Html::beginForm(['/site/logout'], 'post') . $form . Html::endForm();
        $items[] = "<li>{$form}</li>";
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?php
        echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);
        echo Alert::widget();
        echo $content;
        ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
