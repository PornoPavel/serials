<div class="row">
    <form method="GET" action="/">
        <input type="hidden" name="r" value="series/season">
        <div class="form-group">
            <label class="control-label">
                Сериал
                <select name="serial_id" class="form-control">
                    <?php foreach ($serials as $key => $serial): ?>
                        <option value="<?= $key ?>"><?= $serial ?></option>
                    <?php endforeach ?>
                </select>
            </label>
        </div>

        <input type="submit" class="btn btn-primary">
    </form>
</div>