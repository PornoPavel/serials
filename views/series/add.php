<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить серию';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->label('Название') ?>
    <?= $form->field($model, 'description')->textarea()->label('Описание') ?>
    <?= $form->field($model, 'series')->label('Номер серии') ?>
    <?= $form->field($model, 'release_date')->label('Дата выхода') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>