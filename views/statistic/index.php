<script src="/js/Chart.bundle.js"></script>
<script src="/js/Chart.js"></script>


<form method="GET">
    <input type="hidden" name="r" value="statistic/index">
    <div class="row">
        <div class="col-md-3">
            <p>Жанры</p>
            <select name="genre_id">
                <option value="0">Любой</option>
                <?php foreach ($genres as $genre): ?>
                    <option value="<?= $genre['id'] ?>"><?= $genre['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3">
            <p>Страна производителя</p>
            <select name="country_id">
                <option value="0">Любая</option>
                <?php foreach ($countries as $county): ?>
                    <option value="<?= $county['id'] ?>"><?= $county['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3">
            <p>Год выхода</p>
            <select name="year">
                <option value="0">Любая</option>
                <?php foreach ($years as $year): ?>
                    <option value="<?= $year ?>"><?= $year ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3">
            <p>Статусы</p>
            <select name="status_id">
                <option value="0">Любая</option>
                <?php foreach ($statuses as $status): ?>
                    <option value="<?= $status['id'] ?>"><?= $status['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3">
            <p>Студия</p>
            <select name="studio_id">
                <option value="0">Любая</option>
                <?php foreach ($studios as $studio): ?>
                    <option value="<?= $studio['id'] ?>"><?= $studio['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3">
            <p>Теги</p>
            <select name="tag_id">
                <option value="0">Любая</option>
                <?php foreach ($tags as $tag): ?>
                    <option value="<?= $tag['id'] ?>"><?= $tag['name'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <input type="submit" value="Найти">
        </div>
    </div>

</form>

<canvas id="myChart" width="400" height="400"></canvas>

<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels:[ <?php foreach ($serials as $serial):?> '<?= $serial['name'] ?>', <?php endforeach ?>],
            datasets: [{
                label: 'Сериалы',
                data: [ <?php foreach ($serials as $serial):?> '<?= $serial['amount'] ?>', <?php endforeach ?>],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                    }
                }]
            }
        }
    });
</script>