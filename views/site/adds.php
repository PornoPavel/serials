<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div>
        <a href="<?= Url::toRoute(['/country/add']) ?>">Добавить Страну</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/genre/add']) ?>">Добавить Жанр</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/people/add']) ?>">Добавить Актера</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/role/add']) ?>">Добавить Роль</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/status/add']) ?>">Добавить Статус</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/serial/add']) ?>">Добавить Сериал</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/season/add']) ?>">Добавить Сезон</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/series/serial']) ?>">Добавить Серию</a>
    </div>
    <div>
        <a href="<?= Url::toRoute(['/manager/index']) ?>">Добавить актера к серии</a>
    </div>
</div>