<?php
use yii\helpers\Url;
use app\widgets\FilmItem\FilmItem;

$this->title = 'Сериалы';
?>

    <form method="GET">
        <div class="row">
            <div class="col-md-3">
                <p>Жанры</p>
                <select name="genre_id">
                    <option value="0">Любой</option>
                    <?php foreach ($genres as $genre): ?>
                        <option value="<?= $genre['id'] ?>"><?= $genre['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3">
                <p>Страна производителя</p>
                <select name="country_id">
                    <option value="0">Любая</option>
                    <?php foreach ($countries as $county): ?>
                        <option value="<?= $county['id'] ?>"><?= $county['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3">
                <p>Год выхода</p>
                <select name="year">
                    <option value="0">Любая</option>
                    <?php foreach ($years as $year): ?>
                        <option value="<?= $year ?>"><?= $year ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3">
                <p>Статусы</p>
                <select name="status_id">
                    <option value="0">Любая</option>
                    <?php foreach ($statuses as $status): ?>
                        <option value="<?= $status['id'] ?>"><?= $status['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3">
                <p>Студия</p>
                <select name="studio_id">
                    <option value="0">Любая</option>
                    <?php foreach ($studios as $studio): ?>
                        <option value="<?= $studio['id'] ?>"><?= $studio['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3">
                <p>Теги</p>
                <select name="tag_id">
                    <option value="0">Любая</option>
                    <?php foreach ($tags as $tag): ?>
                        <option value="<?= $tag['id'] ?>"><?= $tag['name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <input type="submit" value="Найти">
            </div>
        </div>

    </form>

<div class="row">
    <h1>
        Новые серии
    </h1>
</div>
<?php
$items = [];

foreach ($series as $item) {
    $items[] = [
        'imgSrc' => $item['season']['serial']['image'],
        'text' => $item['name'],
        'name' => $item['season']['serial']['name'],
        'description' => $item['description'],
        'hrefSrc' => Url::toRoute(['serial/index', 'id' => $item['season']['serial']['id']]),
        'lastSeason' => $item['season']['season_num'],
        'lastSeries' => $item['series'],
    ];
}

?>
<div class="row">
    <?php foreach ($items as $item): ?>
        <div class="col-md-4">
            <?= FilmItem::widget(['options' => $item]) ?>
        </div>
    <?php endforeach ?>
</div>
