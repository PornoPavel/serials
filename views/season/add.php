<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить сезон';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'serial_id')->dropDownList($serials)->label('Сериал') ?>
    <?= $form->field($model, 'season_num')->label('Номер сезона') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>