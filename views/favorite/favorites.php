<?php
use yii\helpers\Url;

?>

<?php foreach ($favorites as $favorite): ?>
<div class="row">
    <div class="col-md-12">
        <div class="left-part">
            <img src="<?= $favorite['series']['image'] ?>">
        </div>
        <div class="right-part">
            <a class="title" href="<?= Url::toRoute(['serial/index', 'id' => $favorite['series']['id']]) ?>"><?= $favorite['series']['name'] ?></a>
            <p>
                <strong>
                    <?= $favorite['series']['description'] ?>
                </strong>
            </p>
        </div>
    </div>
</div>
<?php endforeach ?>