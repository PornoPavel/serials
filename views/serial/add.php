<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить сериал';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->label('Название') ?>
    <?= $form->field($model, 'genres')->checkboxList($genres)->label('Жанры') ?>
    <?= $form->field($model, 'tags')->checkboxList($tags)->label('Теги') ?>
    <?= $form->field($model, 'year')->dropDownList($years)->label('Год') ?>
    <?= $form->field($model, 'country_id')->dropDownList($countries)->label('Страна') ?>
    <?= $form->field($model, 'status_id')->dropDownList($statuses)->label('Статусы') ?>
    <?= $form->field($model, 'studio_id')->dropDownList($studios)->label('Студия') ?>
    <?= $form->field($model, 'description')->textarea()->label('Описание') ?>
    <?= $form->field($model, 'image')->fileInput()->label('Картинка') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>