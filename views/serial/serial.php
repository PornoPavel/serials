<?php
use yii\helpers\Url;

?>
<div class="row">
    <div class="col-md-3">
        <img style="max-width: 300px;" src="<?= $serial['image']?>">
        <input type="hidden" name="id" value="<?= $serial['id']?>">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <a href="<?= Url::toRoute(['favorite/add', 'id' => $serial['id']]) ?>">В избранное</a>
            </div>
            <div class="col-md-2"></div>
        <?php endif ?>
    </div>
    <div class="col-md-5">
        <h3><?= $serial['name']?></h3>
        <div>
            Жанры:
            <?php foreach ($serial['genres'] as $genre): ?>
                <span><?= $genre['genre']['name'] ?> </span>
            <?php endforeach ?>
        </div>
        <p>Год выпуска: <span><?= $serial['year']?></span></p>
        <p>
            Роли:
            <?php foreach ($serial['producers'] as $producer): ?>
                <p><?= $producer['FIO'] ?> - <?= $producer['role'] ?> </p>
            <?php endforeach ?>
        </p>
        <p>Описание: <span><?= $serial['description']?></span></p>
        <div class="spacer"></div>
        <p>Сезон:
            <?php foreach ($serial['seasons'] as $season): ?>
                <a href="<?= Url::toRoute(['serial/index', 'id' => $serial['id'], 'season_id' => $season['id']])  ?>"><?= $season['season_num'] ?></a>
            <?php endforeach ?>
        </p>
        <?php foreach ($serial['seasonSelected']['series'] as $series): ?>
        <div class="row">
            <div class="col-md-4">
                <p>Номер серии</p>
                <p><?= $series['series'] ?></p>
            </div>
            <div class="col-md-4">
                <p>Название серии</p>
                <p><?= $series['name'] ?></p>
            </div>
            <div class="col-md-4">
                <p>Дата выхода</p>
                <p><?= $series['release_date'] ?></p>
            </div>
        </div>

        <?php endforeach ?>
    </div>
    <div class="col-md-4">
        <p>Дополнительная информация:</p>
        <p>Статус:<span><?= $serial['status']['name'] ?></span></p>
        <p>Страна производства:
            <?php foreach ($serial['countries'] as $country): ?>
                <span><?= $country['country']['name'] ?> </span>
            <?php endforeach ?>
        </p>
    </div>
</div>