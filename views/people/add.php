<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить жанр';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'FIO')->label('ФИО') ?>
    <?= $form->field($model, 'date_birth')->label('День Рождения') ?>
    <?= $form->field($model, 'biography')->textarea()->label('Биография') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>