<div class="row">
    <form method="GET">
        <input type="hidden" name="r" value="manager/add">
        <div class="form-group">
            <label class="control-label">
                Сезон
                <select name="season_id" class="form-control">
                    <?php foreach ($seasons as $key => $season): ?>
                        <option value="<?= $key ?>"><?= $season ?></option>
                    <?php endforeach ?>
                </select>
            </label>
        </div>

        <input type="submit" class="btn btn-primary">
    </form>
</div>