<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить серию';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'series_id')->dropDownList($series)->label('Серия') ?>
    <?= $form->field($model, 'people_id')->dropDownList($peoples)->label('Человек') ?>
    <?= $form->field($model, 'role_id')->dropDownList($roles)->label('Роль') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>