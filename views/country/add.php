<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить страну';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->label('Имя') ?>
    <div class="row">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>