<?php

namespace app\controllers;

use app\models\form\AddSeasonForm;
use app\models\Season;
use app\models\Serial;
use Yii;
use yii\web\Controller;

class SeasonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddSeasonForm();

        if ($model->load(Yii::$app->request->post())) {
            $serial = new Season();
            $serial->serial_id = $model->serial_id;
            $serial->season_num = $model->season_num;
            $serial->save();
        }

        $serials = [];
        foreach (Serial::find()->all() as $item) {
            $serials[$item['id']] = $item['name'];
        }

        $data = [
            'model' => $model,
            'serials' => $serials,
        ];

        return $this->render('add', $data);
    }
}