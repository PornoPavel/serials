<?php

namespace app\controllers;

use app\models\form\AddSeriesForm;
use app\models\Season;
use app\models\Serial;
use app\models\Series;
use Yii;
use yii\web\Controller;

class SeriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionAdd($season_id)
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddSeriesForm();

        if ($model->load(Yii::$app->request->post())) {
            $series = new Series();
            $series->name = $model->name;
            $series->description = $model->description;
            $series->season_id = $season_id;
            $series->series = $model->series;
            $series->release_date = $model->release_date;

            $series->save();
        }

        $data = [
            'model' => $model,
        ];

        return $this->render('add', $data);
    }

    public function actionSeason($serial_id)
    {
        $serials = [];
        foreach (Season::find()->where(['=', 'serial_id', $serial_id])->all() as $item) {
            $serials[$item['id']] = $item['season_num'];
        }

        $data = [
            'seasons' => $serials,
        ];

        return $this->render('season', $data);
    }

    public function actionSerial()
    {
        $serials = [];
        foreach (Serial::find()->all() as $item) {
            $serials[$item['id']] = $item['name'];
        }

        $data = [
            'serials' => $serials,
        ];

        return $this->render('serial', $data);
    }
}