<?php


namespace app\controllers;


use app\models\form\AddPeopleSeriesModel;
use app\models\form\AddSeriesForm;
use app\models\People;
use app\models\Role;
use app\models\Season;
use app\models\Serial;
use app\models\Series;
use app\models\SeriesPeople;
use Yii;
use yii\web\Controller;

class ManagerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionSeason($serial_id)
    {
        $serials = [];
        foreach (Season::find()->where(['=', 'serial_id', $serial_id])->all() as $item) {
            $serials[$item['id']] = $item['season_num'];
        }

        $data = [
            'seasons' => $serials,
        ];

        return $this->render('season', $data);
    }

    public function actionIndex()
    {
        $serials = [];
        foreach (Serial::find()->all() as $item) {
            $serials[$item['id']] = $item['name'];
        }

        $data = [
            'serials' => $serials,
        ];

        return $this->render('serial', $data);
    }
    public function actionAdd($season_id)
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }
        $series = [];

        foreach (Season::findOne(['id'=>$season_id])->series  as $item) {
            $series[$item['id']] = $item['name'];
        }

        $peoples = [];

        foreach (People::find()->all()  as $item) {
            $peoples[$item['id']] = $item['FIO'];
        }
        $roles = [];

        foreach (Role::find()->all()  as $item) {
            $roles[$item['id']] = $item['name'];
        }

        $model = new AddPeopleSeriesModel();

        if ($model->load(Yii::$app->request->post())) {
            $seriesPeople = new SeriesPeople();
            $seriesPeople->people_id = $model->people_id;
            $seriesPeople->series_id = $model->series_id;
            $seriesPeople->role_id = $model->role_id;

            $seriesPeople->save();
        }

        $data = [
            'model' => $model,
            'series' => $series,
            'peoples' => $peoples,
            'roles' => $roles,
        ];

        return $this->render('add', $data);
    }
}