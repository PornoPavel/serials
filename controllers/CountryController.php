<?php

namespace app\controllers;

use app\models\Country;
use app\models\form\AddCountyModel;
use Yii;
use yii\web\Controller;

class CountryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddCountyModel();

        if ($model->load(Yii::$app->request->post())) {
            $country = new Country();
            $country->name = $model->name;
            $country->save();
        }

        $data = [
            'model' => $model,
        ];

        return $this->render('add', $data);
    }
}