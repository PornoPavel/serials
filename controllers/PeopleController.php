<?php

namespace app\controllers;

use app\models\form\AddPeopleForm;
use app\models\People;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class PeopleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddPeopleForm();

        if ($model->load(Yii::$app->request->post())) {
            $people = new People();
            $people->FIO = $model->FIO;
            $people->date_birth = $model->date_birth;
            $people->biography = $model->biography;
            $people->save();

            $this->redirect(Url::toRoute(['site/adds']));
        }

        $data = [
            'model' => $model,
        ];

        return $this->render('add', $data);
    }
}