<?php

namespace app\controllers;

use app\models\UserFavorite;
use Yii;
use yii\web\Controller;

class FavoriteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $id = Yii::$app->user->identity->id;
        $favorites = UserFavorite::find()->where(['=', 'user_id', $id])->with('series')->asArray()->all();

        return $this->render('favorites', ['favorites' => $favorites,]);
    }

    public function actionAdd($id)
    {
        $userId = Yii::$app->user->identity->getId();

        if (UserFavorite::find()->where(['=', 'serial_id', $id])->where(['=', 'user_id', $userId])->count() < 1) {
            $userFavorite = new UserFavorite();
            $userFavorite->serial_id = $id;
            $userFavorite->user_id = Yii::$app->user->identity->getId();
            $userFavorite->save();
        }

        return $this->redirect(['site/index', 'id' => $id]);
    }
}