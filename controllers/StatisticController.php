<?php

namespace app\controllers;

use app\models\Country;
use app\models\Genres;
use app\models\Serial;
use app\models\Status;
use app\models\Studio;
use app\models\Tag;
use app\models\UserFavorite;
use Yii;
use yii\web\Controller;

class StatisticController extends Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $genres = Genres::find()->all();
        $countries = Country::find()->all();
        $studios = Studio::find()->all();
        $statuses = Status::find()->all();
        $tags = Tag::find()->all();
        $years = range(2020, 1900, 1);

        $serials = Serial::find();

        if ($request->get('genre_id', 0) != 0) {
            $serials->join('LEFT JOIN', 'serials_genres', 'serials_genres.serial_id = serials.id');
            $serials->where(['=', 'serials_genres.genre_id', $request->get('genre_id')]);
        }

        if ($request->get('country_id', 0) != 0) {
            $serials->join('LEFT JOIN', 'serials_countries', 'serials_countries.serial_id = serials.id');
            $serials->where(['=', 'serials_countries.country_id', $request->get('country_id')]);
        }

        if ($request->get('year', 0) != 0) {
            $serials->where(['=', 'year', $request->get('year')]);
        }

        if ($request->get('status_id', 0) != 0) {
            $serials->where(['=', 'status_id', $request->get('status_id')]);
        }

        if ($request->get('studio_id', 0) != 0) {
            $serials->where(['=', 'studio_id', $request->get('studio_id')]);
        }

        if ($request->get('tag_id', 0) != 0) {
            $serials->join('LEFT JOIN', 'tag_serial', 'tag_serial.serial_id = serials.id');
            $serials->where(['=', 'tag_serial.tag_id', $request->get('tag_id')]);
        }

        $serials = $serials->asArray()->all();

        foreach ($serials as &$serial) {
            $serial['amount'] = UserFavorite::find()->where(['=', 'serial_id', $serial['id']])->count();
        }

        $data = [
            'genres' => $genres,
            'countries' => $countries,
            'studios' => $studios,
            'statuses' => $statuses,
            'years' => $years,
            'tags' => $tags,
            'serials' => $serials,
        ];

        return $this->render('index', $data);
    }
}