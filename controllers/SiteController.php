<?php

namespace app\controllers;

use app\models\Country;
use app\models\form\ContactForm;
use app\models\form\LoginForm;
use app\models\form\RegisterFrom;
use app\models\Genres;
use app\models\Season;
use app\models\Serial;
use app\models\Series;
use app\models\Status;
use app\models\Studio;
use app\models\Tag;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $genres = Genres::find()->all();
        $countries = Country::find()->all();
        $studios = Studio::find()->all();
        $statuses = Status::find()->all();
        $tags = Tag::find()->all();

        $series = Series::find()->join('LEFT JOIN', 'seasons', 'seasons.id = series.season_id');
        $series->join('LEFT JOIN', 'serials', 'serials.id = seasons.serial_id');

        if ($request->get('genre_id', 0) != 0) {
            $series->join('LEFT JOIN', 'serials_genres', 'serials_genres.serial_id = serials.id');
            $series->where(['=', 'serials_genres.genre_id', $request->get('genre_id')]);
        }

        if ($request->get('country_id', 0) != 0) {
            $series->join('LEFT JOIN', 'serials_countries', 'serials_countries.serial_id = serials.id');
            $series->where(['=', 'serials_countries.country_id', $request->get('country_id')]);
        }

        if ($request->get('year', 0) != 0) {
            $series->where(['=', 'YEAR(release_date)', $request->get('year')]);
        }

        if ($request->get('status_id', 0) != 0) {
            $series->where(['=', 'status_id', $request->get('status_id')]);
        }

        if ($request->get('studio_id', 0) != 0) {
            $series->where(['=', 'studio_id', $request->get('studio_id')]);
        }

        if ($request->get('tag_id', 0) != 0) {
            $series->join('LEFT JOIN', 'tag_serial', 'tag_serial.serial_id = serials.id');
            $series->where(['=', 'tag_serial.tag_id', $request->get('tag_id')]);
        }

        $series = $series->with('seasonSerial')->with('seasonSerialLast')->asArray()->all();

        foreach ($series as &$item) {
            $item['season'] = Season::find()->where(['=', 'id', $item['season_id']])->asArray()->one();
            $item['season']['serial'] = Serial::find()->where(['=', 'id', $item['season']['serial_id']])->asArray()->one();
        }

        $data = [
            'series' => $series,
            'genres' => $genres,
            'countries' => $countries,
            'studios' => $studios,
            'statuses' => $statuses,
            'years' => range(2020, 1900, -1),
            'tags' => $tags,
        ];

        return $this->render('index', $data);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', ['model' => $model,]);
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterFrom();
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return $this->goHome();
        }

        return $this->render('register', ['model' => $model,]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

        return $this->render('contact', ['model' => $model,]);
    }

    public function actionAdds()
    {
        return $this->render('adds');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
