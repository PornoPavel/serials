<?php

namespace app\controllers;

use app\models\Country;
use app\models\form\AddSerialForm;
use app\models\Genres;
use app\models\People;
use app\models\Role;
use app\models\Season;
use app\models\Serial;
use app\models\SerialGenre;
use app\models\Series;
use app\models\SeriesPeople;
use app\models\Status;
use app\models\Studio;
use app\models\Tag;
use app\models\TagSerial;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class SerialController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddSerialForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');

            $fileName = '/images/' . $model->image->baseName . '.' . $model->image->extension;
            $model->image->saveAs(Yii::$app->basePath . '/web' . $fileName);

            $serial = new Serial();
            $serial->name = $model->name;
            $serial->description = $model->description;
            $serial->year = 2020 - $model->year;
            $serial->status_id = $model->status_id;
            $serial->studio_id = $model->studio_id;
            $serial->image = $fileName;
            $serial->save();

            foreach ($model->genres as $genre) {
                $serialGenre = new SerialGenre();
                $serialGenre->genre_id = $genre;
                $serialGenre->serial_id = $serial->id;
                $serialGenre->save();
            }

            foreach ($model->tags as $tag) {
                $serialGenre = new TagSerial();
                $serialGenre->tag_id = $tag;
                $serialGenre->serial_id = $serial->id;
                $serialGenre->save();
            }
        }

        $genres = [];
        foreach (Genres::find()->all() as $item) {
            $genres[$item['id']] = $item['name'];
        }

        $tags = [];
        foreach (Tag::find()->all() as $item) {
            $tags[$item['id']] = $item['name'];
        }

        $countries = [];
        foreach (Country::find()->all() as $item) {
            $countries[$item['id']] = $item['name'];
        }

        $statuses = [];
        foreach (Status::find()->all() as $item) {
            $statuses[$item['id']] = $item['name'];
        }

        $studios = [];
        foreach (Studio::find()->all() as $item) {
            $studios[$item['id']] = $item['name'];
        }

        $data = [
            'model' => $model,
            'genres' => $genres,
            'tags' => $tags,
            'countries' => $countries,
            'years' => range(2020, 1900, -1),
            'statuses' => $statuses,
            'studios' => $studios,
        ];

        return $this->render('add', $data);
    }

    public function actionIndex($id, $season_id = null)
    {
        $serial = Serial::find()->where(['id' => $id])->with(['genres', 'status', 'countries'])->asArray()->one();

        $producers = People::find()->select([People::tableName() . '.*', SeriesPeople::tableName() . '.*'])
            ->join('LEFT JOIN', SeriesPeople::tableName(), SeriesPeople::tableName() . '.people_id = ' . People::tableName() . '.id')
            ->join('LEFT JOIN', Series::tableName(), SeriesPeople::tableName() . '.series_id = ' . Series::tableName() . '.id')
            ->join('LEFT JOIN', Season::tableName(), Series::tableName() . '.season_id = ' . Season::tableName() . '.id')
            ->join('LEFT JOIN', Serial::tableName(), Season::tableName() . '.serial_id = ' . Serial::tableName() . '.id')
            ->where(['=', Serial::tableName() . '.id', $id])->asArray()->all();

        foreach ($producers as &$item) {
            $item['role'] = Role::findOne(['id' => $item['role_id']])->name;
        }

        $serial['producers'] = $producers;
        $serial['seasons'] = Season::find()->joinWith('series')->where(['=', 'serial_id', $id])->asArray()->all();

        if ($season_id == null) {
            $season_id = 0;
            $serial['seasonSelected'] = $serial['seasons'][0];
        } else {
            $serial['seasonSelected'] = Season::find()->where(['=', 'id', $season_id])->with('series')->asArray()->one();
        }


        return $this->render('serial', ['serial' => $serial, 'season_id' => $season_id]);
    }
}