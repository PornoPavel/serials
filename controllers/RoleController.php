<?php

namespace app\controllers;

use app\models\form\AddRoleModel;
use app\models\Role;
use Yii;
use yii\web\Controller;

class RoleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin == 1) {
            return $this->goHome();
        }

        $model = new AddRoleModel();

        if ($model->load(Yii::$app->request->post())) {
            $role = new Role();
            $role->name = $model->name;
            $role->save();
        }

        $data = [
            'model' => $model,
        ];

        return $this->render('add', $data);
    }
}