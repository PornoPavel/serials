<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%serials_countries}}`.
 */
class m191222_211912_create_serials_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%serials_countries}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->null(),
            'serial_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%serials_countries}}');
    }
}
