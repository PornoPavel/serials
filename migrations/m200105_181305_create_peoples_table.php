<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%peoples}}`.
 */
class m200105_181305_create_peoples_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%peoples}}', [
            'id' => $this->primaryKey(),
            'FIO' => $this->string(255),
            'date_birth' => $this->date(),
            'biography' => $this->string(500),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%peoples}}');
    }
}
