<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%studios}}`.
 */
class m191222_173057_create_studios_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%studios}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%studios}}');
    }
}
