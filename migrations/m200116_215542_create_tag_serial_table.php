<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tag_serial}}`.
 */
class m200116_215542_create_tag_serial_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tag_serial}}', [
            'id' => $this->primaryKey(),
            'serial_id' => $this->integer()->null(),
            'tag_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tag_serial}}');
    }
}
