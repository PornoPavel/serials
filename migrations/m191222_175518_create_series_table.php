<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%series}}`.
 */
class m191222_175518_create_series_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%series}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->null(),
            'season_id' => $this->integer()->null(),
            'release_date' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%series}}');
    }
}
