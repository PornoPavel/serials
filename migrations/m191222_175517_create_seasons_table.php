<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seasons}}`.
 */
class m191222_175517_create_seasons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seasons}}', [
            'id' => $this->primaryKey(),
            'serial_id' => $this->integer()->null(),
            'season_num' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%seasons}}');
    }
}
