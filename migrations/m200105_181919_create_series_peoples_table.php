<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%series_peoples}}`.
 */
class m200105_181919_create_series_peoples_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%series_peoples}}', [
            'id' => $this->primaryKey(),
            'people_id' => $this->integer()->null(),
            'series_id' => $this->integer()->null(),
            'role_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%series_peoples}}');
    }
}
