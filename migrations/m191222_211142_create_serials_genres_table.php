<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%serials_genres}}`.
 */
class m191222_211142_create_serials_genres_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%serials_genres}}', [
            'id' => $this->primaryKey(),
            'genre_id' => $this->integer()->null(),
            'serial_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%serials_genres}}');
    }
}
