<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_favorites}}`.
 */
class m191222_212325_create_users_favorites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users_favorites}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'serial_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_favorites}}');
    }
}
