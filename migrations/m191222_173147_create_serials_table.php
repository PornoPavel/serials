<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%serials}}`.
 */
class m191222_173147_create_serials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%serials}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'status_id' => $this->integer()->null(),
            'studio_id' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%serials}}');
    }
}
